//
//  RepoSearchQuery.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

protocol RepoSearchQuery {
    var language: String { get }
}

struct iOSQuery: RepoSearchQuery {
    var language: String = "swift"
}

struct AndroidQuery: RepoSearchQuery {
    var language: String = "kotlin"
}
