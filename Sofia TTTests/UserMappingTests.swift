//
//  UserMappingTests.swift
//  Sofia TTTests
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import XCTest
@testable import Sofia_TT

class UserMappingTests: XCTestCase {

    var userParser = UserParser()

    func testUserMapping() {
        if let path = Bundle(for: type(of: self)).path(forResource: "user", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let user = try userParser.parse(data: data)
                XCTAssertEqual(user.id, 14101776)
                XCTAssertEqual(user.login, "flutter")
                XCTAssertEqual(user.name, "Flutter")
            } catch let error {
                XCTFail(error.localizedDescription)
            }
        } else {
            XCTFail("user.json not found")
        }
    }

}
