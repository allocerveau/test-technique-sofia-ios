//
//  ContributorsFetching.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation
import PromiseKit

protocol ContributorsFetching {
    func getContributors(of project: String, from owner: String) -> Promise<[Contributor]>
}

extension RemoteService: ContributorsFetching {
    func getContributors(of project: String, from owner: String) -> Promise<[Contributor]> {
        return fetchContributors(of: project, from: owner).map { data in
            let parser = ContributorParser()
            let contributors = try parser.parse(data: data)
            return contributors
        }
    }

    private func fetchContributors(of project: String, from owner: String) -> Promise<Data> {
        return Promise<Data> { seal in

            var urlComponents = URLComponents()
            urlComponents.scheme = Constant.GithubAPI.urlScheme
            urlComponents.host = Constant.GithubAPI.urlHost
            urlComponents.path = Constant.GithubAPI.Contributors.reposPath
                + "/\(owner)"
                + "/\(project)"
                + Constant.GithubAPI.Contributors.contributorsPath
            urlComponents.set(queryParams: ["per_page": "100"])


            guard let url = urlComponents.url else {
                seal.reject(FetchingError.invalidURL)
                return
            }

            let task = session.dataTask(with: url) { (rawData, response, error) in
                if let error = error {
                    seal.reject(error)
                    return
                }

                guard let data = rawData else {
                    seal.reject(FetchingError.dataAbsent)
                    return
                }

                seal.fulfill(data)
            }
            task.resume()
        }
    }
}

