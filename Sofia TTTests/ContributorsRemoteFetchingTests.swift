//
//  ContributorsRemoteFetchingTests.swift
//  Sofia TTTests
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import XCTest
@testable import Sofia_TT

class ContributorsRemoteFetchingTests: XCTestCase {

    let remoteService: ContributorsFetching = RemoteService()

    func testFetchFlutterUser_noError() {
        let expectation = self.expectation(description: "user_fetching_no_error")
        var contributors: [Contributor]?
        let project = "flutter"
        let username = "flutter"

        self.remoteService.getContributors(of: project, from: username).done { fetchedContributors in
            contributors = fetchedContributors
            expectation.fulfill()
        }.catch { error in
            XCTFail(error.localizedDescription)
        }

        waitForExpectations(timeout: 5, handler: nil)
        guard let contributors = contributors else {
            XCTFail("no user fetched")
            return
        }
        XCTAssertFalse(contributors.isEmpty)
    }

}
