//
//  ReposMappingTests.swift
//  Sofia TTTests
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import XCTest
@testable import Sofia_TT

class ReposMappingTests: XCTestCase {

    var repoParser = RepoParser()

    func testUserMapping() {
        if let path = Bundle(for: type(of: self)).path(forResource: "repos", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let reposData = try repoParser.parse(data: data)
                XCTAssertTrue(reposData.items.count == 30)
            } catch let error {
                XCTFail(error.localizedDescription)
            }
        } else {
            XCTFail("repos.json not found")
        }
    }

}
