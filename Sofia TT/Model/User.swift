//
//  User.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

class User: Decodable {
    var id: Int
    var login: String
    var name: String
}

struct UserParser {
    func parse(data: Data) throws -> User {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let userData = try decoder.decode(User.self, from: data)
        return userData
    }
}
