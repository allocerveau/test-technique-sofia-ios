//
//  Contributor.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

class Contributor: Decodable {
    var id: Int
    var login: String
}

struct ContributorParser {
    func parse(data: Data) throws -> [Contributor] {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let contributorsData = try decoder.decode([Contributor].self, from: data)
        return contributorsData
    }
}
