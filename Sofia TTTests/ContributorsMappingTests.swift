//
//  ContributorsMappingTests.swift
//  Sofia TTTests
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import XCTest
@testable import Sofia_TT

class ContributorsMappingTests: XCTestCase {

    var contributorsParser = ContributorParser()

    func testUserMapping() {
        if let path = Bundle(for: type(of: self)).path(forResource: "contributors", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let contributors = try contributorsParser.parse(data: data)
                XCTAssertEqual(contributors.count, 100)
            } catch let error {
                XCTFail(error.localizedDescription)
            }
        } else {
            XCTFail("repos.json not found")
        }
    }

}
