//
//  Extensions.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

extension String {
    func extractISO8601Date() -> Date? {
        let dateFormatter = ISO8601DateFormatter()
        return dateFormatter.date(from: self)
    }
}

extension Date {
    func shortDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: self)
    }
}
