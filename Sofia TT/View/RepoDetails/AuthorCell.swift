//
//  AuthorCell.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import UIKit
import Reusable

class AuthorCell: UITableViewCell, NibReusable {

    @IBOutlet weak var authorAvatarImageView: UIImageView!
    @IBOutlet weak var authorName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        authorAvatarImageView.layer.cornerRadius = authorAvatarImageView.bounds.height / 2
    }

}
