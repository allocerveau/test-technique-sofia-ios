//
//  RepoListTableViewController.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import UIKit
import Reusable

class RepoListTableViewController: UITableViewController {

    var viewModel: RepoListViewModel

    init(viewModel: RepoListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "RepoListTableViewController", bundle: Bundle(for: RepoListTableViewController.self))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(cellType: RepoRowCell.self)
        tableView.register(cellType: NoDataCell.self)
        tableView.register(cellType: LoadingCell.self)

        let platforms = viewModel.allPlatforms

        let segmentedControl = UISegmentedControl(items: platforms.map { $0.rawValue } )
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(platformSegmentedControlValueChanged), for: .valueChanged)
        self.navigationItem.titleView = segmentedControl

        self.title = "Popular \(viewModel.allPlatforms[segmentedControl.selectedSegmentIndex].rawValue) repos"

        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(reload), for: .valueChanged)

        reload()
    }

    @objc private func reload() {
        viewModel.getRepos()
    }

    @objc func platformSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        self.title = "Popular \(viewModel.allPlatforms[sender.selectedSegmentIndex].rawValue) repos"
        let selectedPlatform = viewModel.allPlatforms[sender.selectedSegmentIndex]
        viewModel.changePlatform(to: selectedPlatform)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = viewModel.row(at: indexPath.row)
        switch row {
        case .repo(data: let repoRow):
            let cell: RepoRowCell = tableView.dequeueReusableCell(for: indexPath)
            cell.repoNameLabel.text = repoRow.repoName
            cell.authorNameLabel.text = "by \(repoRow.ownerName)"
            cell.starsCountLabel.text = "\(repoRow.starsCount) ☆"
            cell.licenseNameLabel.text = repoRow.licenseName ?? "License unkown"
            return cell
        case .loading:
            let cell: LoadingCell = tableView.dequeueReusableCell(for: indexPath)
            cell.activityIndicator.startAnimating()
            return cell
        case .noData:
            let cell: NoDataCell = tableView.dequeueReusableCell(for: indexPath)
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = viewModel.row(at: indexPath.row)
        switch row {
        case .repo:
            return 80
        default:
            return 44
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = viewModel.row(at: indexPath.row)
        switch row {
        case .repo(data: let repoRow):
            viewModel.selectedRepo(with: repoRow.id)
        default:
            break
        }
    }
    
}

extension RepoListTableViewController: RepoListViewModelDelegate {

    func updateView() {
        tableView.reloadData()
        tableView.refreshControl?.endRefreshing()
    }

    func displayError(message: String) {
        let alertController = UIAlertController(title: "Error",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
