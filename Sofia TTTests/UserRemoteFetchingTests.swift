//
//  UserRemoteFetchingTests.swift
//  Sofia TTTests
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import XCTest
@testable import Sofia_TT

class UserRemoteFetchingTests: XCTestCase {

    let remoteService: UserFetching = RemoteService()

    func testFetchFlutterUser_noError() {
        let expectation = self.expectation(description: "user_fetching_no_error")
        var user: User?
        let username = "flutter"

        self.remoteService.getUser(with: username).done { fetchedUser in
            user = fetchedUser
            expectation.fulfill()
        }.catch { error in
            XCTFail(error.localizedDescription)
        }

        waitForExpectations(timeout: 5, handler: nil)
        guard let user = user else {
            XCTFail("no user fetched")
            return
        }
        XCTAssertTrue(user.login == username)
    }

}
