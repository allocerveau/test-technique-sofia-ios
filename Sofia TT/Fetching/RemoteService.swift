//
//  RemoteService.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation
import PromiseKit

class RemoteService {

    enum FetchingError: Error {
        case invalidURL
        case invalidJSON
        case dataAbsent
        case parsing
    }

    let session: URLSession

    init(urlSessionConfiguration: URLSessionConfiguration = .default) {
        self.session = URLSession(configuration: urlSessionConfiguration)
    }
}

extension URLComponents {
    mutating func set(queryParams params: [String: String]) {
        self.queryItems = params.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
}
