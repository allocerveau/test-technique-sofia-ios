//
//  StandardCell.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import UIKit
import Reusable

class StandardCell: UITableViewCell, NibReusable {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
