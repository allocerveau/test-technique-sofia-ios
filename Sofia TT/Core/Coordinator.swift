//
//  Coordinator.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation
import UIKit

protocol Coordinator {
    var rootNavigationController: UINavigationController { get set }
    func start()
}

protocol RepoListCoordinator {
    func pushRepoDetails(repo: Repo)
}

class AppCoordinator: Coordinator {
    var rootNavigationController: UINavigationController

    private let window: UIWindow

    public init(window: UIWindow) {
        self.window = window
        self.rootNavigationController = UINavigationController()
        self.window.rootViewController = self.rootNavigationController
    }

    func start() {
        let repoListVM = RepoListViewModel(coordinator: self)
        let repoListTVC = RepoListTableViewController(viewModel: repoListVM)
        repoListVM.viewDelegate = repoListTVC

        self.rootNavigationController.setViewControllers([repoListTVC], animated: false)
        self.rootNavigationController.navigationBar.prefersLargeTitles = true
        self.window.makeKeyAndVisible()
    }
}

extension AppCoordinator: RepoListCoordinator {
    func pushRepoDetails(repo: Repo) {
        let repoDetailsVM = RepoDetailsViewModel(repo: repo)
        let repoDetailsTVC = RepoDetailsTableViewController(viewModel: repoDetailsVM)
        self.rootNavigationController.pushViewController(repoDetailsTVC, animated: true)
    }
}
