//
//  Repo.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

class ReposData: Decodable {
    var totalCount: Int
    var incompleteResults: Bool
    var items: [Repo]
}

class Repo: Decodable {

    class Owner: Decodable {
        var login: String
        var id: Int
        var url: String
        var type: String
        var avatarUrl: String
    }

    class License: Decodable {
        var key: String
        var name: String
    }

    var id: Int
    var name: String
    var fullName: String
    var htmlUrl: String
    var topics: [String]
    var description: String?

    var defaultBranch: String

    var createdAt: String
    var updatedAt: String

    var stargazersCount: Int
    var watchersCount: Int
    var openIssuesCount: Int

    var homepage: String?
    var language: String?

    var license: License?
    var owner: Owner
}

struct RepoParser {
    func parse(data: Data) throws -> ReposData {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let reposData = try decoder.decode(ReposData.self, from: data)
        return reposData
    }
}
