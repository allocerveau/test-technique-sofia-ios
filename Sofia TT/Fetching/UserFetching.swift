//
//  UserFetching.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation
import PromiseKit

protocol UserFetching {
    func getUser(with username: String) -> Promise<User>
}

extension RemoteService: UserFetching {
    func getUser(with username: String) -> Promise<User> {
        return fetchUserData(with: username).map { data in
            let parser = UserParser()
            let user = try parser.parse(data: data)
            return user
        }
    }

    private func fetchUserData(with username: String) -> Promise<Data> {
        return Promise<Data> { seal in

            var urlComponents = URLComponents()
            urlComponents.scheme = Constant.GithubAPI.urlScheme
            urlComponents.host = Constant.GithubAPI.urlHost
            urlComponents.path = Constant.GithubAPI.User.usersPath + "/\(username)"

            guard let url = urlComponents.url else {
                seal.reject(FetchingError.invalidURL)
                return
            }

            let task = session.dataTask(with: url) { (rawData, response, error) in
                if let error = error {
                    seal.reject(error)
                    return
                }

                guard let data = rawData else {
                    seal.reject(FetchingError.dataAbsent)
                    return
                }

                seal.fulfill(data)
            }
            task.resume()
        }
    }
}
