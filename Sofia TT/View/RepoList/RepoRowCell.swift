//
//  RepoRowCell.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import UIKit
import Reusable

class RepoRowCell: UITableViewCell, NibReusable {

    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var starsCountLabel: UILabel!
    @IBOutlet weak var contributorsCountLabel: UILabel!
    @IBOutlet weak var licenseNameLabel: UILabel!

}
