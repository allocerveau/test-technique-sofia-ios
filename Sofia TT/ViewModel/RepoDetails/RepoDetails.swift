//
//  RepoDetails.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

struct RepoDetails {
    var id: Int
    var repoName: String
    var repoFullName: String
    var author: String
    var authorAvatar: String
    var description: String?
    var createdAt: String
    var updatedAt: String
    var watchersCount: Int
    var defaultBranch: String
    var licenseName: String?

    init(from repo: Repo) {
        self.id = repo.id
        self.repoName = repo.name
        self.repoFullName = repo.fullName
        self.author = repo.owner.login
        self.authorAvatar = repo.owner.avatarUrl
        self.description = repo.description
        self.createdAt = repo.createdAt.extractISO8601Date()?.shortDate() ?? "-"
        self.updatedAt = repo.updatedAt.extractISO8601Date()?.shortDate() ?? "-"
        self.watchersCount = repo.watchersCount
        self.defaultBranch = repo.defaultBranch
        self.licenseName? = repo.license?.name ?? "License unknown"
    }
}
