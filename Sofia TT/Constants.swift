//
//  Constants.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

enum Constant {
    enum GithubAPI {
        static var urlScheme: String { "https" }
        static var urlHost: String { "api.github.com" }

        enum Search {
            static var searchPath: String { "/search" }
            static var repositoriesPath: String { "/repositories" }

        }
        enum User {
            static var usersPath: String { "/users" }
        }

        enum Contributors {
            static var reposPath: String { "/repos" }
            static var contributorsPath: String { "/contributors" }
        }
    }
}
