//
//  RepoFetching.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation
import PromiseKit

protocol RepoFetching {
    func getRepos(query: RepoSearchQuery) -> Promise<ReposData>
}

extension RemoteService: RepoFetching {

    func getRepos(query: RepoSearchQuery) -> Promise<ReposData> {
        return fetchReposData(query: query).map { data in
            let parser = RepoParser()
            let reposData = try parser.parse(data: data)
            return reposData
        }
    }

    private func fetchReposData(query: RepoSearchQuery) -> Promise<Data> {
        return Promise<Data> { seal in

            var urlComponents = URLComponents()
            urlComponents.scheme = Constant.GithubAPI.urlScheme
            urlComponents.host = Constant.GithubAPI.urlHost
            urlComponents.path = Constant.GithubAPI.Search.searchPath + Constant.GithubAPI.Search.repositoriesPath
            urlComponents.set(queryParams: ["q": "language:\(query.language)"])

            guard let url = urlComponents.url else {
                seal.reject(FetchingError.invalidURL)
                return
            }

            let task = session.dataTask(with: url) { (rawData, response, error) in
                if let error = error {
                    seal.reject(error)
                    return
                }

                guard let data = rawData else {
                    seal.reject(FetchingError.dataAbsent)
                    return
                }

                seal.fulfill(data)
            }
            task.resume()
        }
    }
}
