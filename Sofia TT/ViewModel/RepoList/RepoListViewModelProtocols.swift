//
//  RepoListViewModelProtocols.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

protocol RepoListViewModelProtocol {
    func row(at index: Int) -> RepoListViewModel.Row
    func changePlatform(to platform: RepoListViewModel.Platform)
    func getRepos()

    func selectedRepo(with id: Int)
}

protocol RepoListViewModelDelegate: AnyObject {
    func updateView()
    func displayError(message: String)
}
