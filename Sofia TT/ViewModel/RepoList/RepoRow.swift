//
//  RepoRow.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

struct RepoRow {
    var id: Int
    var repoName: String
    var ownerName: String
    var licenseName: String?
//    var contributorsCount: Int
    var starsCount: Int

    init(from repo: Repo) {
        self.id = repo.id
        self.repoName = repo.name
        self.ownerName = repo.owner.login
        self.licenseName = repo.license?.name
        self.starsCount = repo.stargazersCount
    }
}
