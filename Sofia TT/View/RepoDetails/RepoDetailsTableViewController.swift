//
//  RepoDetailsTableViewController.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import UIKit
import SDWebImage

class RepoDetailsTableViewController: UITableViewController {

    var viewModel: RepoDetailsViewModel

    init(viewModel: RepoDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "RepoDetailsTableViewController", bundle: Bundle(for: RepoDetailsTableViewController.self))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = viewModel.repoDetails.repoName

        tableView.register(cellType: AuthorCell.self)
        tableView.register(cellType: AboutCell.self)
        tableView.register(cellType: DetailsCell.self)
        tableView.register(cellType: StandardCell.self)

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rows.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = viewModel.rows[indexPath.row]
        let repoDetails = viewModel.repoDetails

        switch row {
        case .author:
            let cell: AuthorCell = tableView.dequeueReusableCell(for: indexPath)
            cell.authorAvatarImageView.sd_setImage(with: URL(string: repoDetails.authorAvatar))
            cell.authorName.text = repoDetails.author
            return cell
        case .about:
            let cell: AboutCell = tableView.dequeueReusableCell(for: indexPath)
            cell.repoFullNameLabel.text = repoDetails.repoFullName
            return cell
        case .description:
            let cell: StandardCell = tableView.dequeueReusableCell(for: indexPath)
            cell.textLabel?.text = repoDetails.description
            cell.textLabel?.numberOfLines = 0
            return cell
        case .details:
            let cell: DetailsCell = tableView.dequeueReusableCell(for: indexPath)
            cell.createdAtLabel.text = "created at " + repoDetails.createdAt
            cell.updatedAtLabel.text = "updated at " + repoDetails.updatedAt
            cell.watchersCountLabel.text = "\(repoDetails.watchersCount) watchers"
            cell.defaultBranchLabel.text = "default branch : " + repoDetails.defaultBranch
            cell.licenseNameLabel.text = repoDetails.licenseName
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = viewModel.rows[indexPath.row]
        switch row {
        case .author:
            return 50
        case .about:
            return 66
        case .details:
            return 135
        case .description:
            return UITableView.automaticDimension
        }
    }
}
