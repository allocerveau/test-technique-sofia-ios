//
//  AboutCell.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import UIKit
import Reusable

class AboutCell: UITableViewCell, NibReusable {

    @IBOutlet weak var repoFullNameLabel: UILabel!

}
