//
//  RepoListViewModel.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation
import PromiseKit

class RepoListViewModel {

    enum Platform: String, CaseIterable {
        case ios = "iOS"
        case android = "Android"
    }
    let allPlatforms: [Platform] = Platform.allCases

    var platform: Platform = .ios {
        didSet {
            switch platform {
            case .ios:
                repoSearchQuery = iOSQuery()
            case .android:
                repoSearchQuery = AndroidQuery()
            }
        }
    }

    enum State {
        case loading
        case data(repos: [Repo])
        case noData
        case error(error: Error)
    }

    private(set) var state: State = .loading {
        didSet {
            switch state {
            case .loading:
                rows = [.loading]
            case .noData:
                rows = [.noData]
            case .data(repos: let repos):
                rows = repos.map { return Row.repo(data: RepoRow(from: $0)) }
            case .error:
                rows = []
                DispatchQueue.main.async {
                    self.viewDelegate?.displayError(message: "An error has occured fetching the repos.")
                }
            }
        }
    }

    enum Row {
        case loading
        case repo(data: RepoRow)
        case noData
    }

    private var rows = [Row]() {
        didSet {
            DispatchQueue.main.async {
                self.viewDelegate?.updateView()
            }
        }
    }
    var numberOfRows: Int {
        get {
            rows.count
        }
    }

    var coordinator: RepoListCoordinator
    weak var viewDelegate: RepoListViewModelDelegate?
    let remoteService = RemoteService()
    var repoSearchQuery: RepoSearchQuery = iOSQuery() {
        didSet {
            self.getRepos()
        }
    }

    init(coordinator: RepoListCoordinator) {
        self.coordinator = coordinator
    }

}

extension RepoListViewModel: RepoListViewModelProtocol {
    func row(at index: Int) -> Row {
        return rows[index]
    }

    func changePlatform(to platform: Platform) {
        self.platform = platform
    }

    func getRepos() {
        state = .loading
        remoteService.getRepos(query: repoSearchQuery).done { [weak self] reposData in
            guard let self = self else { return }

            self.state = .data(repos: reposData.items)

//            var contributorsPromises = [Promise<[Contributor]>]()
//
//            for repo in reposData.items {
//                contributorsPromises.append(self.remoteService.getContributors(of: repo.name, from: repo.owner.login))
//            }
//
//            when(fulfilled: contributorsPromises)
        }.catch { [weak self] error in
            guard let self = self else { return }
            switch error {
            case RemoteService.FetchingError.invalidJSON,
                RemoteService.FetchingError.invalidURL,
                RemoteService.FetchingError.parsing:
                self.state = .error(error: error)
                // TODO : - log it
            default:
                self.state = .error(error: error)
            }
        }
    }

    func selectedRepo(with id: Int) {
        switch state {
        case .data(repos: let repos):
            guard let repo = repos.first(where: { $0.id == id }) else {
                viewDelegate?.displayError(message: "Cannot get this repo details")
                return
            }
            coordinator.pushRepoDetails(repo: repo)
        default:
            // did not select a repo
            break
        }

    }
}
