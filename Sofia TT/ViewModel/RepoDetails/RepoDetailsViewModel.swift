//
//  RepoDetailsViewModel.swift
//  Sofia TT
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import Foundation

class RepoDetailsViewModel {

    let repoDetails: RepoDetails

    enum Row {
        case author
        case about
        case description
        case details
    }
    let rows: [Row]

    init(repo: Repo) {
        self.repoDetails = RepoDetails(from: repo)
        self.rows = [.author, .about, .description, .details]
    }
}

