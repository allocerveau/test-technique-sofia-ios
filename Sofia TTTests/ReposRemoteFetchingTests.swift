//
//  ReposRemoteFetchingTests.swift
//  Sofia TTTests
//
//  Created by Alexandre Guzu on 12/11/2021.
//

import XCTest
@testable import Sofia_TT

class ReposRemoteFetchingTests: XCTestCase {

    var remoteService: RepoFetching = RemoteService()

    func testFetchRepos_ios_noError() {
        fetchRepo(with: iOSQuery())
    }

    func testFetchRepos_android_noError() {
        fetchRepo(with: AndroidQuery())
    }

    func fetchRepo(with query: RepoSearchQuery) {
        let expectation = self.expectation(description: "repos_fetching_no_error")
        var reposData: ReposData?

        self.remoteService.getRepos(query: query).done { fetchedReposData in
            reposData = fetchedReposData
            expectation.fulfill()
        }.catch { error in
            print(error)
            XCTFail(error.localizedDescription)
        }

        waitForExpectations(timeout: 5, handler: nil)
        guard let reposData = reposData else {
            XCTFail("no repo fetched")
            return
        }
        XCTAssertTrue(reposData.items.count == 30)
        for item in reposData.items { XCTAssertEqual(item.language?.lowercased(), query.language.lowercased()) }
    }
}
